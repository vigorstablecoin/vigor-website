var TagManager = require('react-gtm-module');

var tagManagerArgs = {
  gtmId: 'GTM-KHMRPQR'
};

exports.onInitialClientRender = function() {
  TagManager.initialize(tagManagerArgs);
};
