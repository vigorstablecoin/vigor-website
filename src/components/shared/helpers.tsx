import Grid from "styled-components-grid";
import styled from "styled-components";

export const GridCentered = styled(Grid)`
  justify-content: center;
`;
