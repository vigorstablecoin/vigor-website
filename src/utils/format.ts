export const formatPercentage = (value: number, decimals = 2) => {
  return (value * 100).toFixed(decimals)
};
