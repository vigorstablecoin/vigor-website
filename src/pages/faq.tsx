import React from "react";
import { useTranslation } from "react-i18next";
import FaqList from "../components/faq/FaqList";
import Helmet from "react-helmet";
import Hero from '../components/Hero';
import { GridCentered } from '../components/shared';
import Grid from 'styled-components-grid';

const Index: React.FC<{}> = props => {
  const { t } = useTranslation();

  return (
    <React.Fragment>
      <Helmet>
        <meta name="title" content="Vigor Protocol FAQ"/>
        <meta name="description" content="FAQ on the Vigor Protocol DeFi crypto lending platform (VIG and VIGOR tokens)."/>
      </Helmet>

      <GridCentered>
        <Grid.Unit size={{sm: 10/12 }}>
        <Hero
            title={ t(`faqPage.title`) }
            small= { true }
          />

        </Grid.Unit>
      </GridCentered>

      <GridCentered>
        <Grid.Unit size={{sm: 10/12 }}>
          <FaqList />

        </Grid.Unit>
      </GridCentered>
    </React.Fragment>
  );
};

export default Index;
